# Package Tagger

Simple tool used to just append some text to the end of your version in your
package.json.


 ```bash
npm install --save-dev package-tagger
npm install

# Append some text
./node_modules/package-tagger/index.js cool_text

# Append dev.[git_commit_10_char]
git rev-parse develop | cut -c1-10 | xargs -I[] ./node_modules/package-tagger/index.js dev.[]
``` 