#!/usr/bin/env node
var fs = require('fs');
 
const to_append = process.argv[2]

fs.readFile('package.json', function(err, contents) {
  if (err) {
    return console.log(err)
  }
  const p_json = JSON.parse(contents)
  p_json['version'] = p_json['version'] + '-' + to_append
  fs.writeFile('package.json', JSON.stringify(p_json, null, 2), function(err) {
    if (err) {
      return console.log(err)
    }

  })
});
